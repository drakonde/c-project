import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    id: root
    width: 640
    height: 480
    visible: true
    title: qsTr("Task 1")

    Rectangle
    {
        id: scene
        anchors.fill: parent
        state: "InitialState"
                
            Rectangle
            {
                id: left_rec
                x: 100
                y: 200
                color: "lightgrey"
                width: 100
                height: 100
                border.color: "black"
                border.width: 3
                radius: 5
                
                Text
                {
                    id: name_move
                    anchors.centerIn: parent
                    text: "move"
                }
                
                MouseArea
                {
                    anchors.fill: parent
                    onClicked:
                        if(ball.x >= righ_rec.x)
                        {
                            scene.state = "InitialState"
                        }
                        else
                        {
                            ball.x += 25
                            scene.state = "OtherState"
                        }
                }
            }

        Rectangle 
        {
            id: righ_rec
            x: 300
            y: 200
            color: "lightgray"
            width: 100
            height: 100
            border.color: "black"
            border.width: 3
            radius: 5

            Text
            {
                id: name_return
                anchors.centerIn: parent
                text: "return"
            }

            MouseArea 
            {
                anchors.fill: parent
                onClicked: scene.state = "InitialState"
            }
        }

        Rectangle 
        {
            id: ball
            color: "darkgreen"
            x: left_rec.x + 5
            y: left_rec.y + 5
            width: left_rec.width - 10
            height: left_rec.height - 10
            radius: width / 2
        }

        states: 
        [
            State 
            {
                name: "InitialState"
                PropertyChanges 
                {
                    target: ball
                    x: left_rec.x + 5
                }
            },
            State 
            {
                name: "OtherState"
                PropertyChanges 
                {
                    target: ball
                    x: ball.x
                }
            }
        ]

        transitions: 
        [
            Transition 
            {
                from: "OtherState"
                to: "InitialState"

                NumberAnimation 
                {
                    properties: "x,y"
                    duration: 1000
                    easing.type: Easing.OutBounce
                }
            }
        ]
    }
}